package com.selcukbulca.travelbirdassignment.home;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;
import android.test.suitebuilder.annotation.LargeTest;

import com.selcukbulca.travelbirdassignment.R;
import com.selcukbulca.travelbirdassignment.utils.MockLocationProvider;
import com.selcukbulca.travelbirdassignment.utils.MockLocations;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnHolderItem;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.selcukbulca.travelbirdassignment.CustomMatchers.clickChildViewWithId;
import static com.selcukbulca.travelbirdassignment.CustomMatchers.hasItems;
import static com.selcukbulca.travelbirdassignment.CustomMatchers.isVideoPlaying;
import static com.selcukbulca.travelbirdassignment.CustomMatchers.withItemCount;
import static com.selcukbulca.travelbirdassignment.CustomMatchers.withToolbarTitle;
import static com.selcukbulca.travelbirdassignment.CustomMatchers.withViewHolderClass;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

/**
 * Created by selcukbulca on 25/04/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MockHomeActivityTest {

    @Rule
    public ActivityTestRule<HomeActivity> activityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Before
    public void registerIdlingResources() {
        Espresso.registerIdlingResources(
                activityTestRule.getActivity().getCountingIdlingResource());
    }

    @After
    public void unregisterIdlingResources() {
        Espresso.unregisterIdlingResources(
                activityTestRule.getActivity().getCountingIdlingResource());
    }

    @Test
    public void startingActivity_showsHomePageWithItems() {
        String homePageTitle =
                InstrumentationRegistry.getTargetContext().getString(R.string.home_page);
        onView(isAssignableFrom(Toolbar.class))
                .check(matches(withToolbarTitle(is(homePageTitle))));

        onView(withId(R.id.error_button))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.error_text))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.recycler_view))
                .check(matches(allOf(isDisplayed(), hasItems())));
    }

    @Test
    public void clickOnVideo_playsVideo() throws InterruptedException {
        onView(withId(R.id.recycler_view))
                .perform(actionOnHolderItem(
                        withViewHolderClass(HomeAdapter.VideoViewHolder.class),
                        clickChildViewWithId(R.id.video_container)));

        // Lets wait for the video
        Thread.sleep(5000);

        onView(withId(R.id.video_view))
                .check(matches(isVideoPlaying()));
    }

    @Test
    public void changingLocation_changesItems() {
        int itemCount = getItemCountForCurrentLocation();

        onView(withId(R.id.recycler_view))
                .check(matches(allOf(isDisplayed(), hasItems(), withItemCount(itemCount))));

        /** Each click on refresh changes location.
         * More details can be seen at
         * {@link com.selcukbulca.travelbirdassignment.utils.MockLocationProvider} */
        onView(withId(R.id.menu_item_refresh))
                .perform(click());

        itemCount = getItemCountForCurrentLocation();

        onView(withId(R.id.recycler_view))
                .check(matches(allOf(isDisplayed(), hasItems(), withItemCount(itemCount))));
    }

    private int getItemCountForCurrentLocation() {
        // Could be done better
        return MockLocationProvider.getCurrentLocation() == MockLocations.LOCATION_DEFAULT
                ? 4
                : 3;
    }
}
