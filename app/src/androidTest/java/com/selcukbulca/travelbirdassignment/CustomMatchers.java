package com.selcukbulca.travelbirdassignment;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.VideoView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by selcukbulca on 27/04/16.
 */
public final class CustomMatchers {

    private CustomMatchers() {
        // no instance
    }

    public static Matcher<Object> withToolbarTitle(final Matcher<String> textMatcher) {
        return new BoundedMatcher<Object, Toolbar>(Toolbar.class) {
            @Override
            public boolean matchesSafely(Toolbar toolbar) {
                return textMatcher.matches(toolbar.getTitle());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with toolbar title: ");
                textMatcher.describeTo(description);
            }
        };
    }

    public static <S extends RecyclerView.ViewHolder> Matcher<RecyclerView.ViewHolder> withViewHolderClass(final Class<S> viewHolderClass) {
        return new BoundedMatcher<RecyclerView.ViewHolder, S>(viewHolderClass) {
            @Override
            protected boolean matchesSafely(RecyclerView.ViewHolder item) {
                return viewHolderClass.equals(item.getClass());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with viewholder class" + viewHolderClass.getSimpleName());
            }
        };
    }

    public static Matcher<View> hasItems() {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                return item instanceof RecyclerView
                        && ((RecyclerView) item).getAdapter().getItemCount() != 0;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has items in the adapter");
            }
        };
    }

    public static Matcher<View> withItemCount(final int itemCount) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                return item instanceof RecyclerView
                        && ((RecyclerView) item).getAdapter().getItemCount() == itemCount;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has " + itemCount + " items in the adapter");
            }
        };
    }

    public static Matcher<View> isVideoPlaying() {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                return item instanceof VideoView
                        && ((VideoView) item).isPlaying();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is playing video");
            }
        };
    }

    public static ViewAction clickChildViewWithId(final int id) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "click on a child view with specified id";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View childView = view.findViewById(id);
                if (childView != null) {
                    childView.performClick();
                }
            }
        };
    }
}
