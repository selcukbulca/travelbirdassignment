package com.selcukbulca.travelbirdassignment.home;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;
import android.test.suitebuilder.annotation.LargeTest;

import com.selcukbulca.travelbirdassignment.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.selcukbulca.travelbirdassignment.CustomMatchers.withToolbarTitle;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by selcukbulca on 25/04/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class HomeActivityTest {

    @Rule
    public ActivityTestRule<HomeActivity> activityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Before
    public void registerIdlingResources() {
        Espresso.registerIdlingResources(
                activityTestRule.getActivity().getCountingIdlingResource());
    }

    @After
    public void unregisterIdlingResources() {
        Espresso.unregisterIdlingResources(
                activityTestRule.getActivity().getCountingIdlingResource());
    }

    @Test
    public void startingActivity_showsLoginWithInstagram() {
        onView(ViewMatchers.withId(R.id.error_text))
                .check(matches(withText(R.string.error_login)));

        onView(withId(R.id.error_button))
                .check(matches(withText(R.string.login)));
    }

    @Test
    public void clickOnLogin_showsLoginPage() {
        onView(withId(R.id.error_button))
                .perform(click());

        onView(isAssignableFrom(Toolbar.class))
                .check(matches(withToolbarTitle(is("Login"))));

        pressBack();

        onView(isAssignableFrom(Toolbar.class))
                .check(matches(withToolbarTitle(is("Home Page"))));
    }
}
