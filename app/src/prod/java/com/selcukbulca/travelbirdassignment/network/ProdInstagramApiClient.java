package com.selcukbulca.travelbirdassignment.network;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;

/**
 * Created by selcukbulca on 23/04/16.
 */
@Singleton
public class ProdInstagramApiClient implements InstagramApiClient {

    private final InstagramApi instagramApi;

    @Inject
    public ProdInstagramApiClient(Retrofit retrofit) {
        instagramApi = retrofit.create(InstagramApi.class);
    }

    public InstagramApi instagramApi() {
        return instagramApi;
    }
}
