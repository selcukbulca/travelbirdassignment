package com.selcukbulca.travelbirdassignment;

import com.selcukbulca.travelbirdassignment.di.AppComponent;
import com.selcukbulca.travelbirdassignment.di.DaggerProdAppComponent;
import com.selcukbulca.travelbirdassignment.di.ProdAppModule;
import com.selcukbulca.travelbirdassignment.di.ProdNetworkModule;

/**
 * Created by selcukbulca on 26/04/16.
 */
public class App extends BaseApp {

    @Override
    protected AppComponent createAppComponent() {
        return DaggerProdAppComponent.builder()
                .prodAppModule(new ProdAppModule(this))
                .prodNetworkModule(new ProdNetworkModule(BuildConfig.BASE_URL))
                .build();
    }
}
