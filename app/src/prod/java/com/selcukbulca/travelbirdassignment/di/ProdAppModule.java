package com.selcukbulca.travelbirdassignment.di;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.selcukbulca.travelbirdassignment.data.AccessTokenStorage;
import com.selcukbulca.travelbirdassignment.data.ProdAccessTokenStorage;
import com.selcukbulca.travelbirdassignment.utils.LocationProvider;
import com.selcukbulca.travelbirdassignment.utils.ProdLocationProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by selcukbulca on 28/04/16.
 */
@Module
public class ProdAppModule extends AppModule {

    public ProdAppModule(Application application) {
        super(application);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPrefs(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    AccessTokenStorage provideAccessTokenStorage(SharedPreferences sharedPreferences) {
        return new ProdAccessTokenStorage(sharedPreferences);
    }

    @Provides
    @Singleton
    LocationProvider provideLocationProvider() {
        return new ProdLocationProvider(application());
    }
}
