package com.selcukbulca.travelbirdassignment.di;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by selcukbulca on 26/04/16.
 */
@Singleton
@Component(modules = {ProdAppModule.class, ProdNetworkModule.class})
public interface ProdAppComponent extends AppComponent {
}
