package com.selcukbulca.travelbirdassignment.di;

import com.selcukbulca.travelbirdassignment.network.InstagramApiClient;
import com.selcukbulca.travelbirdassignment.network.ProdInstagramApiClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by selcukbulca on 20/04/16.
 */
@Module
public class ProdNetworkModule extends NetworkModule {

    public ProdNetworkModule(String baseUrl) {
        super(baseUrl);
    }

    @Provides
    @Singleton
    InstagramApiClient provideInstagramApiClient(Retrofit retrofit) {
        return new ProdInstagramApiClient(retrofit);
    }
}
