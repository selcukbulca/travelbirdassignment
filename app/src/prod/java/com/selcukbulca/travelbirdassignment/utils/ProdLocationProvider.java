package com.selcukbulca.travelbirdassignment.utils;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import java.util.List;

/**
 * Created by selcukbulca on 26/04/16.
 */
public class ProdLocationProvider implements LocationProvider {

    private final Application application;

    public ProdLocationProvider(Application application) {
        this.application = application;
    }


    @SuppressWarnings("MissingPermission")
    @Override
    public Location getLastKnownLocation() {
        if (!PermissionUtils.locationPermissionGranted(application)) {
            return null;
        }

        LocationManager locationManager = (LocationManager) application.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null) {
            return null;
        }

        List<String> providers = locationManager.getProviders(true);
        if (providers == null) {
            return null;
        }

        Location accurateLocation = null;
        for (String provider : providers) {
            Location location = locationManager.getLastKnownLocation(provider);
            if (location == null) {
                continue;
            }

            if (accurateLocation == null || location.getAccuracy() > accurateLocation.getAccuracy()) {
                accurateLocation = location;
            }
        }

        return accurateLocation;
    }
}
