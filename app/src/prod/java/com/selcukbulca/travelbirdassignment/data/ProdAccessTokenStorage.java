package com.selcukbulca.travelbirdassignment.data;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by selcukbulca on 29/04/16.
 */
@Singleton
public class ProdAccessTokenStorage implements AccessTokenStorage {

    private static final String KEY_ACCESS_TOKEN = "access_token";

    private final SharedPreferences sharedPreferences;

    @Inject
    public ProdAccessTokenStorage(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public boolean exists() {
        return sharedPreferences.contains(KEY_ACCESS_TOKEN);
    }

    @Override
    public String get() {
        return sharedPreferences.getString(KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void save(String accessToken) {
        sharedPreferences.edit()
                .putString(KEY_ACCESS_TOKEN, accessToken)
                .apply();
    }
}
