package com.selcukbulca.travelbirdassignment.login;

import android.content.Intent;
import android.os.Build;

import com.selcukbulca.travelbirdassignment.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by selcukbulca on 29/04/16.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class LoginPresenterTest {

    @Mock
    private LoginView loginView;

    private LoginPresenter loginPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        loginPresenter = new LoginPresenterImpl(loginView);
    }

    @Test
    public void receivingLoginError_notifiesLoginView() {
        loginPresenter.handleUrl(BuildConfig.REDIRECT_URI + "?error=test");

        verify(loginView).onResultIntentCreated(any(Intent.class), eq(true));
    }

    @Test
    public void receivingCode_notifiesLoginView() {
        loginPresenter.handleUrl(BuildConfig.REDIRECT_URI + "?code=test");

        verify(loginView).onResultIntentCreated(any(Intent.class), eq(false));
    }

    @Test
    public void handlingInvalidUrl_returnsFalse() {
        assertThat(loginPresenter.handleUrl("test"), is(false));
    }

}
