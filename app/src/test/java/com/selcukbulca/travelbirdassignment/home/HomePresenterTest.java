package com.selcukbulca.travelbirdassignment.home;

import android.location.Location;

import com.selcukbulca.travelbirdassignment.R;
import com.selcukbulca.travelbirdassignment.data.ProdAccessTokenStorage;
import com.selcukbulca.travelbirdassignment.network.InstagramApiClient;
import com.selcukbulca.travelbirdassignment.utils.LocationProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by selcukbulca on 28/04/16.
 */
public class HomePresenterTest {

    @Mock
    private HomeView homeView;
    @Mock
    private InstagramApiClient client;
    @Mock
    private ProdAccessTokenStorage storage;
    @Mock
    private LocationProvider locationProvider;
    @Mock
    private Location defaultLocation;
    @Mock
    private Location changedLocation;

    private HomePresenterImpl homePresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        homePresenter = new HomePresenterImpl(homeView, client, storage, locationProvider);
    }

    private void logInAndGrantLocationPermission() {
        when(homePresenter.isLoggedIn()).thenReturn(true);
        when(homePresenter.isLocationPermissionGranted()).thenReturn(true);
    }

    @Test
    public void openingWithoutLoggedIn_showsLoginError() {
        homePresenter.onCreate();

        verify(homeView).showError(R.string.error_login);
    }

    @Test
    public void openingWithoutLocationPermission_requestsLocationPermission() {
        when(homePresenter.isLoggedIn()).thenReturn(true);
        when(homePresenter.isLocationPermissionGranted()).thenReturn(false);
        homePresenter.onCreate();

        verify(homeView).requestLocationPermission();
    }

    @Test
    public void openingWithLoggedIn_showsItems() {
        logInAndGrantLocationPermission();
        homePresenter.onCreate();

        verify(homeView).showContent();
    }
}
