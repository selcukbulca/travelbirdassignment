package com.selcukbulca.travelbirdassignment.utils;

import android.location.Location;

import static com.selcukbulca.travelbirdassignment.utils.MockLocations.LOCATIONS;

/**
 * Created by selcukbulca on 28/04/16.
 */
public class MockLocationProvider implements LocationProvider {

    private static int countCalled;

    public static Location getCurrentLocation() {
        return LOCATIONS[countCalled % LOCATIONS.length];
    }

    @Override
    public Location getLastKnownLocation() {
        // Change location for each method call
        return LOCATIONS[++countCalled % LOCATIONS.length];
    }
}
