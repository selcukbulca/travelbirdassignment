package com.selcukbulca.travelbirdassignment.utils;

import android.location.Location;

/**
 * Created by selcukbulca on 28/04/16.
 */
public final class MockLocations {

    // Default mock location
    public static final Location LOCATION_DEFAULT;
    public static final Location LOCATION_CHANGED;

    public static final Location[] LOCATIONS;

    static {
        Location location = new Location("gps");
        location.setLatitude(111111);
        location.setLongitude(111111);
        LOCATION_DEFAULT = location;

        location = new Location("gps");
        location.setLatitude(121212);
        location.setLongitude(121212);
        LOCATION_CHANGED = location;

        LOCATIONS = new Location[]{LOCATION_DEFAULT, LOCATION_CHANGED};
    }

    private MockLocations() {
        // no instance
    }
}
