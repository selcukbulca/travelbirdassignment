package com.selcukbulca.travelbirdassignment.data;

/**
 * Created by selcukbulca on 29/04/16.
 */
public class MockAccessTokenStorage implements AccessTokenStorage {

    @Override
    public boolean exists() {
        return true;
    }

    @Override
    public String get() {
        return "mock";
    }

    @Override
    public void save(String accessToken) {
        // no-op
    }
}
