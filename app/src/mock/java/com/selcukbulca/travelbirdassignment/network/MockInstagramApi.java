package com.selcukbulca.travelbirdassignment.network;

import com.google.gson.Gson;
import com.selcukbulca.travelbirdassignment.network.model.MediaDataBundle;
import com.selcukbulca.travelbirdassignment.network.model.UserCredentials;

import java.util.concurrent.TimeUnit;

import retrofit2.http.Field;
import retrofit2.http.Query;
import rx.Observable;

import static com.selcukbulca.travelbirdassignment.network.MockResponses.SEARCH_RESPONSE_LOCATION_CHANGED;
import static com.selcukbulca.travelbirdassignment.network.MockResponses.SEARCH_RESPONSE_LOCATION_DEFAULT;
import static com.selcukbulca.travelbirdassignment.utils.MockLocations.LOCATION_DEFAULT;

/**
 * Created by selcukbulca on 26/04/16.
 */
public class MockInstagramApi implements InstagramApi {

    private final Gson gson;

    public MockInstagramApi(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Observable<UserCredentials> accessToken(@Field("client_id") String clientId, @Field("client_secret") String clientSecret, @Field("grant_type") String grantType, @Field("redirect_uri") String redirectUri, @Field("code") String code) {
        return Observable.just(gson.fromJson(MockResponses.USER_CREDENTIALS_RESPONSE, UserCredentials.class))
                .delay(2, TimeUnit.SECONDS);
    }

    @Override
    public Observable<MediaDataBundle> search(@Query("access_token") String accessToken, @Query("lat") double latitude, @Query("lng") double longitude, @Query("distance") int distance) {
        return Observable.just(gson.fromJson(isLocationDefault(latitude, longitude) ? SEARCH_RESPONSE_LOCATION_DEFAULT : SEARCH_RESPONSE_LOCATION_CHANGED, MediaDataBundle.class))
                .delay(2, TimeUnit.SECONDS);
    }

    private boolean isLocationDefault(double latitude, double longitude) {
        return LOCATION_DEFAULT.getLatitude() == latitude
                && LOCATION_DEFAULT.getLongitude() == longitude;
    }
}
