package com.selcukbulca.travelbirdassignment.network;

/**
 * Created by selcukbulca on 27/04/16.
 */
final class MockResponses {

    static final String USER_CREDENTIALS_RESPONSE = "{\n" +
            "  \"access_token\": \"mock\"\n" +
            "}";

    static final String SEARCH_RESPONSE_LOCATION_DEFAULT = "{\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"caption\": {\n" +
            "        \"text\": \"Mock caption 1\"\n" +
            "      },\n" +
            "      \"images\": {\n" +
            "        \"thumbnail\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://lorempixel.com/150/150/sports/1\",\n" +
            "          \"width\": 150\n" +
            "        },\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 320,\n" +
            "          \"url\": \"http://lorempixel.com/320/320/sports/1\",\n" +
            "          \"width\": 320\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://lorempixel.com/640/640/sports/1\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"caption\": {\n" +
            "        \"text\": \"Mock caption 2\"\n" +
            "      },\n" +
            "      \"images\": {\n" +
            "        \"thumbnail\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://lorempixel.com/150/150/sports/2\",\n" +
            "          \"width\": 150\n" +
            "        },\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 320,\n" +
            "          \"url\": \"http://lorempixel.com/320/320/sports/2\",\n" +
            "          \"width\": 320\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://lorempixel.com/640/640/sports/2\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"caption\": null,\n" +
            "      \"images\": {\n" +
            "        \"thumbnail\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://lorempixel.com/150/150/sports/3\",\n" +
            "          \"width\": 150\n" +
            "        },\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 320,\n" +
            "          \"url\": \"http://lorempixel.com/320/320/sports/3\",\n" +
            "          \"width\": 320\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://lorempixel.com/640/640/sports/3\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"images\": {\n" +
            "        \"thumbnail\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://lorempixel.com/150/150/sports/4\",\n" +
            "          \"width\": 150\n" +
            "        },\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 320,\n" +
            "          \"url\": \"http://lorempixel.com/320/320/sports/4\",\n" +
            "          \"width\": 320\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://lorempixel.com/640/640/sports/4\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"videos\": {\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 360,\n" +
            "          \"url\": \"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4\",\n" +
            "          \"width\": 240\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"type\": \"video\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    static final String SEARCH_RESPONSE_LOCATION_CHANGED = "{\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"caption\": {\n" +
            "        \"text\": \"Mock caption 1\"\n" +
            "      },\n" +
            "      \"images\": {\n" +
            "        \"thumbnail\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://lorempixel.com/150/150/animals/1\",\n" +
            "          \"width\": 150\n" +
            "        },\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 320,\n" +
            "          \"url\": \"http://lorempixel.com/320/320/animals/1\",\n" +
            "          \"width\": 320\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://lorempixel.com/640/640/animals/1\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"caption\": {\n" +
            "        \"text\": \"Mock caption 2\"\n" +
            "      },\n" +
            "      \"images\": {\n" +
            "        \"thumbnail\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://lorempixel.com/150/150/animals/2\",\n" +
            "          \"width\": 150\n" +
            "        },\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 320,\n" +
            "          \"url\": \"http://lorempixel.com/320/320/animals/2\",\n" +
            "          \"width\": 320\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://lorempixel.com/640/640/animals/2\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"images\": {\n" +
            "        \"thumbnail\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://lorempixel.com/150/150/animals/4\",\n" +
            "          \"width\": 150\n" +
            "        },\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 320,\n" +
            "          \"url\": \"http://lorempixel.com/320/320/animals/4\",\n" +
            "          \"width\": 320\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://lorempixel.com/640/640/animals/4\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"videos\": {\n" +
            "        \"low_resolution\": {\n" +
            "          \"height\": 360,\n" +
            "          \"url\": \"http://techslides.com/demos/sample-videos/small.mp4\",\n" +
            "          \"width\": 240\n" +
            "        },\n" +
            "        \"standard_resolution\": {\n" +
            "          \"height\": 640,\n" +
            "          \"url\": \"http://techslides.com/demos/sample-videos/small.mp4\",\n" +
            "          \"width\": 640\n" +
            "        }\n" +
            "      },\n" +
            "      \"type\": \"video\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    private MockResponses() {
        // no instance
    }
}
