package com.selcukbulca.travelbirdassignment.network;

import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by selcukbulca on 26/04/16.
 */
@Singleton
public class MockInstagramApiClient implements InstagramApiClient {

    private final Gson gson;

    @Inject
    public MockInstagramApiClient(Gson gson) {
        this.gson = gson;
    }

    @Override
    public InstagramApi instagramApi() {
        return new MockInstagramApi(gson);
    }
}
