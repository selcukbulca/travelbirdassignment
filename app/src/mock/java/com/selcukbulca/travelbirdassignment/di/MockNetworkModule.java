package com.selcukbulca.travelbirdassignment.di;

import com.google.gson.Gson;
import com.selcukbulca.travelbirdassignment.network.InstagramApiClient;
import com.selcukbulca.travelbirdassignment.network.MockInstagramApiClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by selcukbulca on 20/04/16.
 */
@Module
public class MockNetworkModule extends NetworkModule {

    public MockNetworkModule(String baseUrl) {
        super(baseUrl);
    }

    @Provides
    @Singleton
    InstagramApiClient provideInstagramApiClient(Gson gson) {
        return new MockInstagramApiClient(gson);
    }
}
