package com.selcukbulca.travelbirdassignment.di;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by selcukbulca on 27/04/16.
 */
@Singleton
@Component(modules = {MockAppModule.class, MockNetworkModule.class})
public interface MockAppComponent extends AppComponent {
}
