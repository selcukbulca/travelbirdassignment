package com.selcukbulca.travelbirdassignment.di;

import android.app.Application;

import com.selcukbulca.travelbirdassignment.data.AccessTokenStorage;
import com.selcukbulca.travelbirdassignment.data.MockAccessTokenStorage;
import com.selcukbulca.travelbirdassignment.utils.LocationProvider;
import com.selcukbulca.travelbirdassignment.utils.MockLocationProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by selcukbulca on 28/04/16.
 */
@Module
public class MockAppModule extends AppModule {

    public MockAppModule(Application application) {
        super(application);
    }

    @Provides
    @Singleton
    AccessTokenStorage provideAccessTokenStorage() {
        return new MockAccessTokenStorage();
    }

    @Provides
    @Singleton
    LocationProvider provideLocationProvider() {
        return new MockLocationProvider();
    }
}
