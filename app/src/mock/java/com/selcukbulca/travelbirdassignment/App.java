package com.selcukbulca.travelbirdassignment;

import com.selcukbulca.travelbirdassignment.di.AppComponent;
import com.selcukbulca.travelbirdassignment.di.DaggerMockAppComponent;
import com.selcukbulca.travelbirdassignment.di.MockAppModule;
import com.selcukbulca.travelbirdassignment.di.MockNetworkModule;

/**
 * Created by selcukbulca on 26/04/16.
 */
public class App extends BaseApp {

    @Override
    protected AppComponent createAppComponent() {
        return DaggerMockAppComponent.builder()
                .mockAppModule(new MockAppModule(this))
                .mockNetworkModule(new MockNetworkModule(""))
                .build();
    }
}
