package com.selcukbulca.travelbirdassignment.network.model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class MediaDataBundle {

    private List<MediaData> data;

    public List<MediaData> getData() {
        return data;
    }

    public String toJson() {
        return new Gson().toJson(data);
    }
}
