package com.selcukbulca.travelbirdassignment.network;

import com.selcukbulca.travelbirdassignment.network.model.MediaDataBundle;
import com.selcukbulca.travelbirdassignment.network.model.UserCredentials;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by selcukbulca on 20/04/16.
 */
public interface InstagramApi {

    @FormUrlEncoded
    @POST("oauth/access_token")
    Observable<UserCredentials> accessToken(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("grant_type") String grantType,
            @Field("redirect_uri") String redirectUri,
            @Field("code") String code
    );

    @GET("v1/media/search")
    Observable<MediaDataBundle> search(
            @Query("access_token") String accessToken,
            @Query("lat") double latitude,
            @Query("lng") double longitude,
            @Query("distance") int distance
    );
}
