package com.selcukbulca.travelbirdassignment.network;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.selcukbulca.travelbirdassignment.network.model.MediaType;

import java.io.IOException;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class MediaTypeAdapter extends TypeAdapter<MediaType> {
    
    @Override
    public void write(JsonWriter out, MediaType value) throws IOException {
        if (value == null) {
            out.nullValue();
        } else {
            out.value(value.getType());
        }
    }

    @Override
    public MediaType read(JsonReader in) throws IOException {
        switch (in.peek()) {
            case NULL:
                in.nextNull();
                return null;
            default:
                return MediaType.from(in.nextString());
        }
    }
}
