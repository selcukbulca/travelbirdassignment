package com.selcukbulca.travelbirdassignment.network.model;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class Media {

    private String url;
    private int width;
    private int height;

    public String getUrl() {
        return url;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
