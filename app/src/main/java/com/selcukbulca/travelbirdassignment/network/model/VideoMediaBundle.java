package com.selcukbulca.travelbirdassignment.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class VideoMediaBundle {

    @SerializedName("low_resolution")
    private Media lowResolution;
    @SerializedName("standard_resolution")
    private Media standardResolution;

    public Media lowResolution() {
        return lowResolution;
    }

    public Media standardResolution() {
        return standardResolution;
    }
}
