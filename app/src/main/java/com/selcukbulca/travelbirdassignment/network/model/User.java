package com.selcukbulca.travelbirdassignment.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class User {

    private String id;
    private String username;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("profile_picture")
    private String profilePicture;

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return fullName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }
}
