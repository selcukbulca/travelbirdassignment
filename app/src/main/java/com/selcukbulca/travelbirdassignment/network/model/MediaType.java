package com.selcukbulca.travelbirdassignment.network.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by selcukbulca on 24/04/16.
 */
public enum MediaType {
    IMAGE("image"),
    VIDEO("video");

    private static final Map<String, MediaType> VALUES_MAP = new HashMap<>();

    static {
        for (MediaType mediaType : values()) {
            VALUES_MAP.put(mediaType.getType(), mediaType);
        }
    }

    public static MediaType from(String type) {
        return VALUES_MAP.get(type);
    }

    private final String type;

    MediaType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
