package com.selcukbulca.travelbirdassignment.network;

/**
 * Created by selcukbulca on 26/04/16.
 */
public interface InstagramApiClient {

    InstagramApi instagramApi();
}
