package com.selcukbulca.travelbirdassignment.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class UserCredentials {

    @SerializedName("access_token")
    private String accessToken;
    private User user;

    public String getAccessToken() {
        return accessToken;
    }

    public User getUser() {
        return user;
    }
}
