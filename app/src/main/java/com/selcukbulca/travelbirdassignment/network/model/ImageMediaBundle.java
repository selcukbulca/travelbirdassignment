package com.selcukbulca.travelbirdassignment.network.model;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class ImageMediaBundle extends VideoMediaBundle {

    private Media thumbnail;

    public Media thumbnail() {
        return thumbnail;
    }
}
