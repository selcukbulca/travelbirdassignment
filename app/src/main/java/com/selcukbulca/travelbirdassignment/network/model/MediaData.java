package com.selcukbulca.travelbirdassignment.network.model;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class MediaData {

    private MediaType type;
    private ImageMediaBundle images;
    private VideoMediaBundle videos;
    private Caption caption;

    public MediaType getType() {
        return type;
    }

    public ImageMediaBundle getImages() {
        return images;
    }

    public VideoMediaBundle getVideos() {
        return videos;
    }

    public Caption getCaption() {
        return caption;
    }
}
