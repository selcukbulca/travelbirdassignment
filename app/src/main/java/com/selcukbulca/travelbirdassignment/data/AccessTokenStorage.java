package com.selcukbulca.travelbirdassignment.data;

/**
 * Created by selcukbulca on 29/04/16.
 */
public interface AccessTokenStorage {

    boolean exists();

    String get();

    void save(String accessToken);
}
