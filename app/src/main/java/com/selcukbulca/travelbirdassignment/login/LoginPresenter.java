package com.selcukbulca.travelbirdassignment.login;

import com.selcukbulca.travelbirdassignment.base.BasePresenter;

/**
 * Created by selcukbulca on 23/04/16.
 */
public interface LoginPresenter extends BasePresenter {

    boolean handleUrl(String url);
}
