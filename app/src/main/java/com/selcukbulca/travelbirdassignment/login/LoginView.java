package com.selcukbulca.travelbirdassignment.login;

import android.content.Intent;

/**
 * Created by selcukbulca on 23/04/16.
 */
interface LoginView {

    void onResultIntentCreated(Intent resultIntent, boolean error);
}
