package com.selcukbulca.travelbirdassignment.login;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.selcukbulca.travelbirdassignment.BuildConfig;
import com.selcukbulca.travelbirdassignment.base.BasePresenterImpl;

/**
 * Created by selcukbulca on 23/04/16.
 */
final class LoginPresenterImpl extends BasePresenterImpl<LoginView> implements LoginPresenter {

    private static final String KEY_CODE = "code";
    private static final String KEY_ERROR = "error";

    LoginPresenterImpl(@Nullable LoginView view) {
        super(view);
    }

    @Override
    public boolean handleUrl(String url) {
        if (!isViewPresent()) {
            return false;
        }

        if (url.startsWith(BuildConfig.REDIRECT_URI)) {
            Uri uri = Uri.parse(url);
            String code = uri.getQueryParameter(KEY_CODE);
            String error = uri.getQueryParameter(KEY_ERROR);

            if (code != null) {
                view().onResultIntentCreated(createIntent(KEY_CODE, code), false);
            } else if (error != null) {
                view().onResultIntentCreated(createIntent(KEY_ERROR, error), true);
            }
            return true;
        }

        return false;
    }

    private Intent createIntent(String key, String value) {
        Intent intent = new Intent();
        intent.putExtra(key, value);
        return intent;
    }
}
