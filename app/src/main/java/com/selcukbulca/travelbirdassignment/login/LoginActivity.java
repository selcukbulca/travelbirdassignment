package com.selcukbulca.travelbirdassignment.login;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.selcukbulca.travelbirdassignment.BuildConfig;
import com.selcukbulca.travelbirdassignment.R;
import com.selcukbulca.travelbirdassignment.base.BaseActivity;

import butterknife.BindView;

/**
 * Created by selcukbulca on 23/04/16.
 */
public class LoginActivity extends BaseActivity<LoginPresenter> implements LoginView {

    public static Intent newIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @BindView(R.id.web_view)
    WebView webView;

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenterImpl(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load instagram authentication url
        webView.loadUrl(BuildConfig.AUTH_URL);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return presenter().handleUrl(url);
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResultIntentCreated(Intent resultIntent, boolean error) {
        setResult(error ? RESULT_CANCELED : RESULT_OK, resultIntent);
        finish();
    }
}
