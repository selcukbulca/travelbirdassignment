package com.selcukbulca.travelbirdassignment.di;

import android.app.Application;

import com.selcukbulca.travelbirdassignment.home.HomeActivity;
import com.selcukbulca.travelbirdassignment.home.HomePresenterImpl;
import com.selcukbulca.travelbirdassignment.network.InstagramApiClient;
import com.selcukbulca.travelbirdassignment.utils.LocationProvider;

/**
 * Created by selcukbulca on 18/04/16.
 *
 * Base app component for prod and mock app components
 */
public interface AppComponent {

    void inject(HomeActivity activity);

    void inject(HomePresenterImpl homePresenter);

    Application application();

    InstagramApiClient client();

    LocationProvider locationProvider();
}
