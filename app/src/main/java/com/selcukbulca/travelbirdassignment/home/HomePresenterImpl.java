package com.selcukbulca.travelbirdassignment.home;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.selcukbulca.travelbirdassignment.BuildConfig;
import com.selcukbulca.travelbirdassignment.R;
import com.selcukbulca.travelbirdassignment.base.BasePresenterImpl;
import com.selcukbulca.travelbirdassignment.data.AccessTokenStorage;
import com.selcukbulca.travelbirdassignment.network.InstagramApiClient;
import com.selcukbulca.travelbirdassignment.network.model.MediaDataBundle;
import com.selcukbulca.travelbirdassignment.network.model.UserCredentials;
import com.selcukbulca.travelbirdassignment.utils.EspressoIdlingResource;
import com.selcukbulca.travelbirdassignment.utils.LocationProvider;
import com.selcukbulca.travelbirdassignment.utils.PermissionUtils;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by selcukbulca on 23/04/16.
 */
public class HomePresenterImpl extends BasePresenterImpl<HomeView> implements HomePresenter {

    private static final int REQUEST_CODE_LOGIN_ACTIVITY = 100;
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 101;

    private static final String KEY_ERROR = "error";
    private static final String KEY_CODE = "code";

    private String accessToken;

    private final InstagramApiClient client;
    private final LocationProvider locationProvider;
    private final AccessTokenStorage accessTokenStorage;

    public HomePresenterImpl(@Nullable HomeView view, InstagramApiClient client,
                             AccessTokenStorage accessTokenStorage, LocationProvider locationProvider) {
        super(view);
        this.client = client;
        this.accessTokenStorage = accessTokenStorage;
        this.locationProvider = locationProvider;
    }

    @Override
    public void onCreate() {
        if (!isViewPresent()) {
            return;
        }

        if (isLoggedIn()) {
            accessToken = accessTokenStorage.get();
            if (isLocationPermissionGranted()) {
                fetchRecentMediaItems();
            } else {
                view().requestLocationPermission();
            }
        } else {
            view().showError(R.string.error_login);
        }
    }

    @VisibleForTesting
    boolean isLocationPermissionGranted() {
        return view().isLocationPermissionGranted();
    }

    @VisibleForTesting
    boolean isLoggedIn() {
        return accessTokenStorage.exists();
    }

    @Override
    public void onRefresh() {
        fetchRecentMediaItems();
    }

    @Override
    public void onErrorButtonClicked() {
        if (!isViewPresent()) {
            return;
        }

        if (accessToken == null) {
            view().navigateToLogin(REQUEST_CODE_LOGIN_ACTIVITY);
        } else if (!isLocationPermissionGranted()) {
            view().requestLocationPermission();
        } else {
            fetchRecentMediaItems();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!isViewPresent()) {
            return;
        }

        if (requestCode == REQUEST_CODE_LOGIN_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                fetchAccessToken(data.getStringExtra(KEY_CODE));
            } else if (resultCode == Activity.RESULT_CANCELED) {
                if (data != null && data.hasExtra(KEY_ERROR)) {
                    view().setErrorButtonText(R.string.retry);
                    view().showError(data.getStringExtra(KEY_ERROR));
                } else {
                    view().setErrorButtonText(R.string.login);
                    view().showError(R.string.error_login);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (!isViewPresent()) {
            return;
        }

        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            if (PermissionUtils.verifyPermissions(grantResults)) {
                fetchRecentMediaItems();
            } else {
                view().showError(R.string.error_location_permission);
                view().setErrorButtonText(R.string.retry);
            }
        }
    }

    private void showContentRefreshing() {
        view().showContent();
        view().setRefreshing(true);
    }

    public void fetchAccessToken(String code) {
        if (!isViewPresent()) {
            return;
        }

        showContentRefreshing();

        EspressoIdlingResource.increment();

        client.instagramApi()
                .accessToken(
                        BuildConfig.CLIENT_ID,
                        BuildConfig.CLIENT_SECRET,
                        "authorization_code",
                        BuildConfig.REDIRECT_URI,
                        code
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userCredentialsSubscriber());
    }

    private Subscriber<UserCredentials> userCredentialsSubscriber() {
        return new Subscriber<UserCredentials>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                EspressoIdlingResource.decrement();
                e.printStackTrace();
                if (isViewPresent()) {
                    view().showError(e.getMessage());
                }
            }

            @Override
            public void onNext(UserCredentials userCredentials) {
                EspressoIdlingResource.decrement();

                accessToken = userCredentials.getAccessToken();
                accessTokenStorage.save(accessToken);

                fetchRecentMediaItems();
            }
        };
    }

    public void fetchRecentMediaItems() {
        if (!isViewPresent()
                // TODO: Check this out
                && !EspressoIdlingResource.getIdlingResource().isIdleNow()) {
            return;
        }

        showContentRefreshing();

        Location location = getLocation();
        if (location == null) {
            view().showError(R.string.error_location_permission);
            view().setErrorButtonText(R.string.retry);
            return;
        }

        EspressoIdlingResource.increment();

        client.instagramApi()
                .search(
                        accessToken,
                        location.getLatitude(),
                        location.getLongitude(),
                        5000
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mediaBundleSubscriber());
    }

    @VisibleForTesting
    Location getLocation() {
        return locationProvider.getLastKnownLocation();
    }

    private Subscriber<MediaDataBundle> mediaBundleSubscriber() {
        return new Subscriber<MediaDataBundle>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                EspressoIdlingResource.decrement();
                e.printStackTrace();
                if (isViewPresent()) {
                    view().showError(e.getMessage());
                }
            }

            @Override
            public void onNext(MediaDataBundle bundle) {
                EspressoIdlingResource.decrement();
                if (isViewPresent()) {
                    view().setListItems(bundle);
                }
            }
        };
    }
}
