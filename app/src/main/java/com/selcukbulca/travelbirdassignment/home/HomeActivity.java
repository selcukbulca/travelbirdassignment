package com.selcukbulca.travelbirdassignment.home;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.selcukbulca.travelbirdassignment.BaseApp;
import com.selcukbulca.travelbirdassignment.R;
import com.selcukbulca.travelbirdassignment.base.BaseActivity;
import com.selcukbulca.travelbirdassignment.data.AccessTokenStorage;
import com.selcukbulca.travelbirdassignment.login.LoginActivity;
import com.selcukbulca.travelbirdassignment.network.InstagramApiClient;
import com.selcukbulca.travelbirdassignment.network.model.MediaDataBundle;
import com.selcukbulca.travelbirdassignment.utils.EspressoIdlingResource;
import com.selcukbulca.travelbirdassignment.utils.LocationProvider;
import com.selcukbulca.travelbirdassignment.utils.PermissionUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by selcukbulca on 23/04/16.
 */
public class HomeActivity extends BaseActivity<HomePresenter> implements HomeView, SwipeRefreshLayout.OnRefreshListener {

    private static final int REQUEST_CODE_LOCATION_PERMISSION = 101;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.error_layout)
    RelativeLayout errorLayout;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.error_button)
    Button errorButton;

    @Inject
    AccessTokenStorage accessTokenStorage;
    @Inject
    InstagramApiClient client;
    @Inject
    LocationProvider locationProvider;

    @Override
    protected HomePresenter createPresenter() {
        return new HomePresenterImpl(this, client, accessTokenStorage, locationProvider);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseApp.from(this).component().inject(this);
        presenter().onCreate();

        swipeRefreshLayout.setColorSchemeResources(R.color.accent_color);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void initToolbar(Toolbar toolbar) {
        super.initToolbar(toolbar);
        toolbar.setTitle(R.string.home_page);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_refresh:
                setRefreshing(true);
                presenter().onRefresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void navigateToLogin(int requestCode) {
        startActivityForResult(LoginActivity.newIntent(this), requestCode);
    }

    @Override
    public void setRefreshing(final boolean refreshing) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(refreshing);
            }
        });
    }

    @Override
    public void showContent() {
        errorLayout.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String error) {
        errorLayout.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setVisibility(View.GONE);
        errorText.setText(error);
        // Clear items in the list
        recyclerView.setAdapter(null);
    }

    @Override
    public void showError(@StringRes int errorStringRes) {
        setRefreshing(false);
        showError(getString(errorStringRes));
    }

    @Override
    public void setListItems(MediaDataBundle bundle) {
        setRefreshing(false);
        recyclerView.setAdapter(new HomeAdapter(this, bundle.getData()));
    }

    @Override
    public void setErrorButtonText(@StringRes int buttonTextStringRef) {
        errorButton.setText(buttonTextStringRef);
    }

    @Override
    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_CODE_LOCATION_PERMISSION);
    }

    @Override
    public boolean isLocationPermissionGranted() {
        return PermissionUtils.locationPermissionGranted(this);
    }

    @Override
    public void onRefresh() {
        presenter().onRefresh();
    }

    @OnClick(R.id.error_button)
    protected void onErrorButtonClicked() {
        presenter().onErrorButtonClicked();
    }

    @VisibleForTesting
    IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}
