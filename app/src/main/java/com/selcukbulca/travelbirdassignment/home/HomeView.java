package com.selcukbulca.travelbirdassignment.home;

import android.support.annotation.StringRes;

import com.selcukbulca.travelbirdassignment.network.model.MediaDataBundle;

/**
 * Created by selcukbulca on 23/04/16.
 */
public interface HomeView {

    void navigateToLogin(int requestCode);

    void setRefreshing(boolean refreshing);

    void showContent();

    void showError(String error);

    void showError(@StringRes int errorStringRes);

    void setListItems(MediaDataBundle bundle);

    void setErrorButtonText(@StringRes int buttonTextStringRef);

    void requestLocationPermission();

    boolean isLocationPermissionGranted();
}
