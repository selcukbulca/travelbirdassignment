package com.selcukbulca.travelbirdassignment.home;

import android.content.Intent;

import com.selcukbulca.travelbirdassignment.base.BasePresenter;

/**
 * Created by selcukbulca on 23/04/16.
 */
public interface HomePresenter extends BasePresenter {

    void onCreate();

    void onRefresh();

    void onErrorButtonClicked();

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults);
}
