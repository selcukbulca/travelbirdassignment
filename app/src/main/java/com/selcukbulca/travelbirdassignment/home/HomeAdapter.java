package com.selcukbulca.travelbirdassignment.home;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.selcukbulca.travelbirdassignment.R;
import com.selcukbulca.travelbirdassignment.network.model.Caption;
import com.selcukbulca.travelbirdassignment.network.model.MediaData;
import com.selcukbulca.travelbirdassignment.network.model.MediaType;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by selcukbulca on 24/04/16.
 */
public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_IMAGE_FULL_WIDTH = R.id.view_type_image_full_width;
    private static final int VIEW_TYPE_IMAGE_HALF_WIDTH = R.id.view_type_image_half_width;
    private static final int VIEW_TYPE_VIDEO = R.id.view_type_image_video;

    private final List<MediaData> items;
    private final LayoutInflater layoutInflater;

    HomeAdapter(Context context, List<MediaData> items) {
        this.items = items;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).getType() == MediaType.VIDEO) {
            return VIEW_TYPE_VIDEO;
        } else if (position % 2 == 0) { // Alternate between full and half width
            return VIEW_TYPE_IMAGE_FULL_WIDTH;
        } else {
            return VIEW_TYPE_IMAGE_HALF_WIDTH;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_VIDEO) {
            return createVideoViewHolder(parent);
        } else if (viewType == VIEW_TYPE_IMAGE_FULL_WIDTH) {
            return createFullWidthViewHolder(parent);
        } else {
            return createHalfWidthImageViewHolder(parent);
        }
    }

    @NonNull
    private VideoViewHolder createVideoViewHolder(ViewGroup parent) {
        return new VideoViewHolder(layoutInflater.inflate(R.layout.item_video, parent, false));
    }

    @NonNull
    private FullWidthImageViewHolder createFullWidthViewHolder(ViewGroup parent) {
        return new FullWidthImageViewHolder(layoutInflater.inflate(R.layout.item_image_full_width, parent, false));
    }

    @NonNull
    private HalfWidthImageViewHolder createHalfWidthImageViewHolder(ViewGroup parent) {
        return new HalfWidthImageViewHolder(layoutInflater.inflate(R.layout.item_image_half_width, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final MediaData item = items.get(position);
        if (holder instanceof VideoViewHolder) {
            bindVideoViewHolder((VideoViewHolder) holder, item);
        } else if (holder instanceof FullWidthImageViewHolder) {
            bindFullWidthImageViewHolder((FullWidthImageViewHolder) holder, item);
        } else {
            bindHalfWidthImageViewHolder((HalfWidthImageViewHolder) holder, item);
        }
    }

    private void bindVideoViewHolder(final VideoViewHolder holder, final MediaData item) {
        holder.videoView.setVideoURI(Uri.parse(item.getVideos().lowResolution().getUrl()));
    }

    private void bindFullWidthImageViewHolder(FullWidthImageViewHolder holder, MediaData item) {
        Glide.with(holder.image.getContext())
                .load(item.getImages().lowResolution().getUrl())
                .into(holder.image);
    }

    private void bindHalfWidthImageViewHolder(HalfWidthImageViewHolder holder, MediaData item) {
        Glide.with(holder.image.getContext())
                .load(item.getImages().lowResolution().getUrl())
                .into(holder.image);

        final Caption caption = item.getCaption();
        if (caption != null && !TextUtils.isEmpty(caption.getText())) {
            holder.caption.setText(caption.getText());
        }
    }

    @VisibleForTesting
    static final class VideoViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.video_view)
        VideoView videoView;
        @BindView(R.id.video_play_button)
        ImageView playButton;
        boolean playing;

        public VideoViewHolder(View itemView) {
            super(itemView);

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playButton.setVisibility(View.VISIBLE);
                    playing = false;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (playing) {
                        playButton.setVisibility(View.VISIBLE);
                        videoView.pause();
                    } else {
                        playButton.setVisibility(View.GONE);
                        videoView.start();
                    }
                    playing = !playing;
                }
            });
        }
    }

    @VisibleForTesting
    static final class HalfWidthImageViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.image_half_width)
        ImageView image;
        @BindView(R.id.caption)
        TextView caption;

        public HalfWidthImageViewHolder(View itemView) {
            super(itemView);
        }
    }

    @VisibleForTesting
    static final class FullWidthImageViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.image_full_width)
        ImageView image;

        public FullWidthImageViewHolder(View itemView) {
            super(itemView);
        }
    }

    private static abstract class ButterKnifeViewHolder extends RecyclerView.ViewHolder {

        public ButterKnifeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
