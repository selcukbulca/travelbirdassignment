package com.selcukbulca.travelbirdassignment;

import android.app.Application;
import android.content.Context;

import com.selcukbulca.travelbirdassignment.di.AppComponent;

/**
 * Created by selcukbulca on 18/04/16.
 */
public abstract class BaseApp extends Application {

    public static BaseApp from(Context context) {
        return (BaseApp) context.getApplicationContext();
    }

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = createAppComponent();
    }

    protected abstract AppComponent createAppComponent();

    public AppComponent component() {
        return component;
    }
}
