package com.selcukbulca.travelbirdassignment.base;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import java.lang.ref.WeakReference;

/**
 * Created by selcukbulca on 23/04/16.
 */
public abstract class BasePresenterImpl<V> implements BasePresenter {

    @VisibleForTesting
    public WeakReference<V> viewRef;

    public BasePresenterImpl(@Nullable V view) {
        this.viewRef = new WeakReference<>(view);
    }

    protected V view() {
        return viewRef.get();
    }

    protected boolean isViewPresent() {
        return viewRef.get() != null;
    }

    public void onDestroy() {
        viewRef.clear();
    }
}
