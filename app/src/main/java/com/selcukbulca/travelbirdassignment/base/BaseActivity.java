package com.selcukbulca.travelbirdassignment.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.selcukbulca.travelbirdassignment.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by selcukbulca on 23/04/16.
 */
public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    private T presenter;

    protected final T presenter() {
        if (presenter == null) {
            presenter = createPresenter();
        }

        return presenter;
    }

    protected abstract T createPresenter();

    @LayoutRes
    protected abstract int getLayoutResId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);

        initToolbar(toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onDestroy() {
        presenter().onDestroy();
        super.onDestroy();
    }

    protected void initToolbar(Toolbar toolbar) {

    }
}
