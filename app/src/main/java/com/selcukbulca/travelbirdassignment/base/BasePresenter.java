package com.selcukbulca.travelbirdassignment.base;

/**
 * Created by selcukbulca on 23/04/16.
 */
public interface BasePresenter {

    void onDestroy();
}
