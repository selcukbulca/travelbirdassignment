package com.selcukbulca.travelbirdassignment.utils;

import android.location.Location;

/**
 * Created by selcukbulca on 26/04/16.
 */
public interface LocationProvider {

    Location getLastKnownLocation();
}
